import "../Styles/footer.scss";

const Footer = () => {
  const footerLinks = (element) => {
    const allLinks = document.querySelectorAll(".footerLinks");
    switch (element) {
      case 0:
        allLinks.forEach((item, index) =>
          index === 0 ? (item.style.opacity = 1) : (item.style.opacity = 0.2)
        );
        break;
      case 1:
        allLinks.forEach((item, index) =>
          index === 1 ? (item.style.opacity = 1) : (item.style.opacity = 0.2)
        );
        break;
      case 2:
        allLinks.forEach((item, index) =>
          index === 2 ? (item.style.opacity = 1) : (item.style.opacity = 0.2)
        );
        break;
      case 3:
        allLinks.forEach((item, index) =>
          index === 3 ? (item.style.opacity = 1) : (item.style.opacity = 0.2)
        );
        break;
      case 4:
        allLinks.forEach((item, index) =>
          index === 4 ? (item.style.opacity = 1) : (item.style.opacity = 0.2)
        );
        break;
      case 5:
        allLinks.forEach((item, index) =>
          index === 5 ? (item.style.opacity = 1) : (item.style.opacity = 0.2)
        );
        break;
      default:
        allLinks.forEach((item) => (item.style.opacity = 1));
        break;
    }
  };

  const link_move_normal = () => {
    const allLinks = document.querySelectorAll(".footerLinks");
    allLinks.forEach((item) => (item.style.opacity = 1));
  };

  return (
    <>
      <footer>
        <h1>
          Let's
          <br />
          #ConnectOnCutting?
        </h1>

        <aside onMouseLeave={link_move_normal}>
          <a
            href="/"
            className="footerLinks"
            onMouseEnter={() => footerLinks(0)}
            data-cursorpointermini={true}
          >
            Home
          </a>
          <a
            href="/story"
            className="footerLinks"
            onMouseEnter={() => footerLinks(1)}
            data-cursorpointermini={true}
          >
            Story
          </a>
          <a
            href="/media"
            className="footerLinks"
            onMouseEnter={() => footerLinks(2)}
            data-cursorpointermini={true}
          >
            Media
          </a>
          <a
            href="/franchise"
            className="footerLinks"
            onMouseEnter={() => footerLinks(3)}
            data-cursorpointermini={true}
          >
            Franchise
          </a>
          <a
            href="/events"
            className="footerLinks"
            onMouseEnter={() => footerLinks(4)}
            data-cursorpointermini={true}
          >
            Events
          </a>
          <a
            href="/chaiwalacares"
            className="footerLinks"
            onMouseEnter={() => footerLinks(5)}
            data-cursorpointermini={true}
          >
            Chai Wala Cares
          </a>
        </aside>

        <div>
          <h5>Email</h5>
          <a href="mailTo:Info@chaiwala.com">Info@chaiwala.com</a>
          <a href="mailTo:Franchise@chaiwala.com">Franchise@chaiwala.com</a>
        </div>

        <div>
          <h5>Phone</h5>
          <a href="tel:123456789">+123456789</a>
        </div>

        <p>© COPYRIGHT 2023 MBA CHAIWALA. MADE BY</p>
      </footer>
      <div className="footer"></div>
    </>
  );
};

export default Footer;
