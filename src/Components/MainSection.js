import "../Styles/Section.scss";
import { easeIn, motion } from "framer-motion";
const MainSection = ({
  h3,
  text,
  hasBtn = true,
  btnText,
  imgSrc,
  imgSize = "70%",
  backgroundColor,
  headingColor,
  textColor,
  btnBgColor,
  btnColor,
}) => {
  const headerAnimation = {
    initial: {
      y: "-100%",
      opacity: 0,
    },
    whileInView: {
      y: "0%",
      opacity: 1,
    },
  };
  const textAnimation = {
    ...headerAnimation,
    transition: {
      delay: 0.3,
    },
  };

  const buttonAnimation = {
    initial: {
      y: "100%",
      opacity: 0,
    },
    whileInView: {
      y: "0%",
      opacity: 1,
    },
    transition: {
      delay: 0.3,
      ease: easeIn,
    },
  };

  const imageAnimation = {
    ...buttonAnimation,
    initial: {
      y: "100%",
      opacity: 0,
    },
    whileInView: {
      y: "0%",
      opacity: 1,
    },
    transition: {
      delay: 0.2,
      ease: easeIn,
    },
  };
  return (
    <section
      className="section"
      style={{
        backgroundColor,
      }}
    >
      <div>
        <motion.h3
          style={{
            color: headingColor,
          }}
          data-cursorpointer={true}
          {...headerAnimation}
        >
          {h3}
        </motion.h3>
        <motion.p
          style={{
            color: textColor,
          }}
          {...textAnimation}
        >
          {text}
        </motion.p>
        {hasBtn && (
          <motion.button
            {...buttonAnimation}
            style={{
              color: btnColor,
              backgroundColor: btnBgColor,
            }}
            data-cursorpointer={true}
          >
            {btnText}
          </motion.button>
        )}
        <motion.div {...imageAnimation}>
          <img
            src={imgSrc}
            alt="imgSrc"
            style={{
              width: imgSize,
            }}
          />
        </motion.div>
      </div>
    </section>
  );
};
export default MainSection;
