import { useEffect } from "react";
import "./App.css";
import IntroVideo from "./Components/IntroVideo";
import MainSection from "./Components/MainSection";
import freshTopicImg from "../src/assets/academy.png";
import freshTopicImg2 from "../src/assets/story.png";
import tedTalkImg from "../src/assets/in-the-news.gif";
import franchiseImg from "../src/assets/franchise.gif";
import mapImg from "../src/assets/locations.png";
import coursesImg from "../src/assets/image2.png";
import albumImg from "../src/assets/mba-cares.gif";
import baratImg from "../src/assets/image1.png";
import chaiwalaImg from "../src/assets/image3.png";
import data from "../src/data/data.json";
import Footer from "./Components/Footer";
import Misc from "./Components/Misc";
import "../src/Styles/mediaQuery.scss";

function App() {
  const yellow = "#fff100";
  const pink = "#ed1e79";
  const white = " #fff";
  const brown = "#6d3d0f";

  const cursorMove = (e) => {
    const cursor = document.querySelector(".cursor");
    cursor.style.top = `${e.pageY - 14}px`;
    cursor.style.left = `${e.pageX - 14}px`;
    const element = e.target;
    if (element.getAttribute("data-cursorpointer")) {
      cursor.classList.add("cursorHover");
    } else if (element.getAttribute("data-cursorpointermini")) {
      cursor.classList.add("cursorHoverMini");
    } else {
      cursor.classList.remove("cursorHover");
      cursor.classList.remove("cursorHoverMini");
    }
  };

  useEffect(() => {
    window.addEventListener("mousemove", cursorMove);
    return () => {
      window.removeEventListener("mousemove", cursorMove);
    };
  }, []);

  return (
    <div>
      <IntroVideo />
      {/* freshTopic */}
      <MainSection
        h3={data.freshTopic.heading}
        text={data.freshTopic.text}
        btnText={data.freshTopic.btn}
        imgSrc={freshTopicImg}
        backgroundColor={pink}
        headingColor={yellow}
        textColor={yellow}
        btnBgColor={yellow}
        btnColor={pink}
      />
      {/* freshTopic2 */}
      <MainSection
        h3={data.freshTopic2.heading}
        text={data.freshTopic2.text}
        btnText={data.freshTopic2.btn}
        imgSrc={freshTopicImg2}
        backgroundColor={pink}
        headingColor={yellow}
        textColor={yellow}
        btnBgColor={yellow}
        btnColor={pink}
      />
      {/* tedTalks */}
      <MainSection
        h3={data.tedTalks.heading}
        text={data.tedTalks.text}
        btnText={data.tedTalks.btn}
        imgSrc={tedTalkImg}
        backgroundColor={yellow}
        headingColor={pink}
        textColor={pink}
        btnBgColor={pink}
        btnColor={yellow}
      />
      {/* franchise */}
      <MainSection
        h3={data.franchise.heading}
        text={data.franchise.text}
        btnText={data.franchise.btn}
        imgSrc={franchiseImg}
        backgroundColor={white}
        headingColor={pink}
        textColor={brown}
        btnBgColor={brown}
        btnColor={yellow}
      />
      {/* map */}
      <MainSection
        h3={data.map.heading}
        text={data.map.text}
        imgSrc={mapImg}
        hasBtn={false}
        backgroundColor={pink}
        headingColor={yellow}
        textColor={yellow}
        btnBgColor={brown}
        btnColor={yellow}
      />
      {/* courses */}
      <MainSection
        h3={data.courses.heading}
        text={data.courses.text}
        btnText={data.courses.btn}
        imgSrc={coursesImg}
        imgSize={"30%"}
        backgroundColor={yellow}
        headingColor={pink}
        textColor={pink}
        btnBgColor={pink}
        btnColor={yellow}
      />
      {/* album */}
      <MainSection
        h3={data.album.heading}
        text={data.album.text}
        btnText={data.album.btn}
        imgSrc={albumImg}
        backgroundColor={white}
        headingColor={pink}
        textColor={brown}
        btnBgColor={brown}
        btnColor={yellow}
      />
      {/* barat */}
      <MainSection
        h3={data.barat.heading}
        text={data.barat.text}
        btnText={data.barat.btn}
        imgSrc={baratImg}
        backgroundColor={pink}
        headingColor={yellow}
        textColor={yellow}
        btnBgColor={yellow}
        btnColor={pink}
      />
      {/* chaiwala */}
      <MainSection
        h3={data.chaiwala.heading}
        text={data.chaiwala.text}
        btnText={data.chaiwala.btn}
        imgSrc={chaiwalaImg}
        backgroundColor={white}
        headingColor={pink}
        textColor={brown}
        btnBgColor={brown}
        btnColor={yellow}
      />
      <Footer />
      <Misc />
    </div>
  );
}

export default App;
